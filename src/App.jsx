//css imports
import "./App.css";

import { Routes, Route, Navigate } from "react-router-dom";
//components
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";

import { useSelector } from "react-redux";
import { selectIsLoggedIn } from "./selectors";
function App() {
  const isLoggedIn = useSelector(selectIsLoggedIn);
  return (
    <>
      <Routes>
        <Route path="/" element={isLoggedIn ? <Home /> : <Navigate to={"/login"} />} />
        <Route path="/login" element={!isLoggedIn ? <Login /> : <Navigate to={"/"} />} />
      </Routes>
    </>
  );
}

export default App;
