const initialState = {
  isLoggedIn: false,
  userName: "",
  password: "",
  valid: false,
};
function loginReducer(state = initialState, action) {
  switch (action.type) {
    case "LOGIN":
      return { ...state, isLoggedIn: true };
    case "LOGOUT":
      return { ...state, isLoggedIn: false };
    case "SET_USERNAME":
      return { ...state, userName: action.payload };
    case "SET_PASSWORD":
      return { ...state, password: action.payload };
    case "CHECK_IS_VALID":
      return { ...state, valid: action.payload };
    default:
      return state;
  }
}
export default loginReducer;
