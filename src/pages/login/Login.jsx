/* eslint-disable react/prop-types */
import "./login.css";
import { login, setUsername, setPassword, checkIsValid } from "../../actions/authAction";
import { useDispatch, useSelector } from "react-redux";
import { selectUsername, selectPassword, selectIsIncorrect } from "../../selectors";
function Login() {
  const dispatch = useDispatch();
  const username = useSelector(selectUsername);
  const password = useSelector(selectPassword);
  const isIncorrect = useSelector(selectIsIncorrect);

  const sumbitForm = (e) => {
    e.preventDefault();
    if (username === "parsa" && password === "1234") dispatch(login());
    else {
      dispatch(checkIsValid(true));
      dispatch(setUsername(""));
      dispatch(setPassword(""));
    }
  };
  return (
    <div className="form-container">
      <form className="login-form" onSubmit={sumbitForm}>
        <h2 className="login-title">Login Now!</h2>
        <p className="login-subtitle">username is parsa and the password is 1234 😁</p>

        <div className="form-content">
          <label className="login-label">
            <span>username: </span>
            <input
              type="text"
              value={username}
              style={isIncorrect ? { border: "1px solid red" } : {}}
              onChange={(e) => dispatch(setUsername(e.target.value))}
              required
            />
          </label>
          <label className="login-label">
            <span>password: </span>
            <input
              type="text"
              value={password}
              style={isIncorrect ? { border: "1px solid red" } : {}}
              onChange={(e) => dispatch(setPassword(e.target.value))}
              required
            />
          </label>
        </div>
        <input className="submit-login-btn" type="submit" />
        {isIncorrect && <p className="invalid-login">incorrect username or password</p>}
      </form>
    </div>
  );
}

export default Login;
