/* eslint-disable react/prop-types */
import { useDispatch } from "react-redux";
import "./checkbox.css";
import { markAsRead } from "../../actions/todoActions";
function Checkbox({ todo }) {
  const dispatch = useDispatch();
  const clickCheckbox = () => {
    dispatch(markAsRead(todo.id));
  };

  return (
    <div
      className="checkbox"
      style={todo.read ? { background: "var(--main)" } : {}}
      onClick={clickCheckbox}
    ></div>
  );
}

export default Checkbox;
