/* eslint-disable react/prop-types */
import "./filter.css";

function Filter({ type, number, onFilter }) {
  return (
    <div className="filter-box" onClick={() => onFilter(type)}>
      <div className="filter-num-box">
        <span>{number}</span>
      </div>
      <span>{type}</span>
    </div>
  );
}

export default Filter;
