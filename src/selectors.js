export const selectIsLoggedIn = (state) => state.auth.isLoggedIn;
export const selectUsername = (state) => state.auth.userName;
export const selectPassword = (state) => state.auth.password;
export const selectIsIncorrect = (state) => state.auth.valid;
//todo
export const selectTodos = (state) => state.todo.todos;
export const selectTodoText = (state) => state.todo.todoText;
export const selectEditedText = (state) => state.todo.editedText;
export const selectFilteredTodos = (state) => state.todo.filteredTodos;
export const selectEditMode = (state) => state.todo.editMode;
export const selectAddBtn = (state) => state.todo.addBtnClicked;
export const selectIsTodoEmpty = (state) => state.todo.isTodoEmpty;
