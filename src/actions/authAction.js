export const login = () => {
  return { type: "LOGIN" };
};
export const logout = () => {
  return { type: "LOGOUT" };
};
export const setUsername = (username) => {
  return { type: "SET_USERNAME", payload: username };
};
export const setPassword = (password) => {
  return { type: "SET_PASSWORD", payload: password };
};
export const checkIsValid = (value) => {
  return { type: "CHECK_IS_VALID", payload: value };
};
